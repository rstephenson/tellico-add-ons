<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tc="http://periapsis.org/tellico/"
                exclude-result-prefixes="tc"
                version="1.0">

<!--
   ===================================================================
   Tellico XSLT file - used for exporting to JSONFeed

   Copyright (C) 2022 Robby Stephenson <robby@periapsis.org>

   This XSLT stylesheet is designed to be used with the 'Tellico'
   application, which can be found at https://tellico-project.org

   ===================================================================
-->

<xsl:output method="text" />

<xsl:variable name="endl">
<xsl:text>
</xsl:text>
</xsl:variable>

<xsl:template match="/">
 <xsl:apply-templates select="tc:tellico"/>
</xsl:template>

<xsl:template match="tc:tellico">
 <xsl:text>{</xsl:text>
 <xsl:value-of select="$endl"/>
 <xsl:text>"version":"https://jsonfeed.org/version/1.1",</xsl:text>
 <xsl:value-of select="$endl"/>
 <xsl:apply-templates select="tc:collection"/>
 <xsl:value-of select="$endl"/>
</xsl:template>

<xsl:template match="tc:collection[not(@type=2) and not(@type=5)]">
 <xsl:message terminate="yes">
  <xsl:text>Export is only appropriate for book collections and bibliographies.</xsl:text>
 </xsl:message>
</xsl:template>

<xsl:template match="tc:collection[@type=2 or @type=5]">
 <xsl:text>"title":"</xsl:text>
 <xsl:value-of select="@title"/>
 <xsl:value-of select="concat('&quot;,',$endl)"/>

 <xsl:text>"items":[</xsl:text>
 <xsl:value-of select="$endl"/>
 <xsl:for-each select="tc:entry">
  <xsl:if test="position()&gt;1">
   <xsl:value-of select="concat(',',$endl)"/>
  </xsl:if>
  <xsl:value-of select="concat(' {',$endl)"/>
  <xsl:apply-templates select="."/>
  <xsl:value-of select="concat($endl,' }')"/>
 </xsl:for-each>
 <xsl:value-of select="$endl"/>
 <xsl:text>]}</xsl:text>
</xsl:template>

<xsl:template match="tc:entry">
 <xsl:text>  "id":"</xsl:text>
 <xsl:value-of select="@id"/>
 <xsl:value-of select="concat('&quot;,',$endl)"/>
 
 <xsl:text>  "content_text":"</xsl:text>
 <xsl:value-of select="tc:title[1]"/>
 <xsl:value-of select="concat('&quot;,',$endl)"/>

 <xsl:if test="tc:isbn">
  <xsl:text>  "url":"https://micro.blog/books/</xsl:text>
  <xsl:value-of select="translate(tc:isbn,translate(tc:isbn, '0123456789Xx', ''), '')"/>
  <xsl:value-of select="concat('&quot;,',$endl)"/>
 </xsl:if>

 <xsl:text>  "date_published":"</xsl:text>
 <xsl:call-template name="datetime">
  <xsl:with-param name="date" select="(tc:cdate|tc:mdate)[1]"/>
 </xsl:call-template>
 <xsl:value-of select="concat('&quot;,',$endl)"/>

 <xsl:text>  "authors":[</xsl:text>
 <xsl:value-of select="$endl"/>
 <xsl:for-each select="tc:authors/tc:author">
  <xsl:if test="position()&gt;1">
   <xsl:value-of select="concat(',',$endl)"/>
  </xsl:if>
  <xsl:text>   {"name":"</xsl:text>
  <xsl:value-of select="."/>
  <xsl:text>"}</xsl:text>
 </xsl:for-each>
 <xsl:value-of select="concat($endl,'  ]')"/>
</xsl:template>

<xsl:template name="datetime">
 <xsl:param name="date"/>
 <xsl:value-of select="concat($date/tc:year,'-',
                       $date/tc:month,'-',
                       $date/tc:day,'T11:59:59+00:00')"/>
</xsl:template>

</xsl:stylesheet>
<!-- Local Variables: -->
<!-- sgml-indent-step: 1 -->
<!-- sgml-indent-data: 1 -->
<!-- End: -->
