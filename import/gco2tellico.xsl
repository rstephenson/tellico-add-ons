<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tc="http://periapsis.org/tellico/"
                version="1.0">

<!--
   ===================================================================
   Tellico XSLT file - used for importing data from GCO

   Copyright (C) Robby Stephenson <robby@periapsis.org>

   This XSLT stylesheet is designed to be used with the 'Tellico'
   application, which can be found at http://tellico-project.org

   ===================================================================
-->

<xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"
            doctype-public="-//Robby Stephenson/DTD Tellico V11.0//EN"
            doctype-system="http://periapsis.org/tellico/dtd/v11/tellico.dtd"/>

<!-- by default, don't output text -->
<xsl:template match="text()"/>

<xsl:template match="/">
 <tc:tellico syntaxVersion="11">
  <tc:collection title="GCO Import" type="6">
   <tc:fields>
    <tc:field name="_default"/>
    <xsl:if test="comics/comic/location">
     <tc:field flags="6" title="Location" category="Personal" format="4" type="1" name="location" i18n="true"/>
    </xsl:if>
    <xsl:if test="comics/comic/price">
     <tc:field flags="0" title="Cover Price" category="Publishing" format="4" type="1" name="cover_price" i18n="true"/>
    </xsl:if>
    <xsl:if test="comics/comic/current_price">
     <tc:field flags="0" title="Current Price" category="Publishing" format="4" type="1" name="curr_price" i18n="true"/>
    </xsl:if>
   </tc:fields>
   <xsl:apply-templates select="comics/comic/issue"/>
  </tc:collection>
 </tc:tellico>
</xsl:template>

<xsl:template match="issue">
 <tc:entry id="{@guid}">
  <xsl:apply-templates/>
  <tc:publisher>
   <xsl:value-of select="../publisher"/>
  </tc:publisher>
  <tc:series>
   <xsl:value-of select="../title"/>
  </tc:series>
  <!-- combine penciller and inker in artist field -->
  <tc:artists>
   <xsl:for-each select="(inkers/inker | pencillers/penciller)">
    <tc:artist>
     <xsl:value-of select="."/>
    </tc:artist>
   </xsl:for-each>
  </tc:artists>
  <tc:keywords>
   <tc:keyword>
    <xsl:value-of select="../type"/>
   </tc:keyword>
   <tc:keyword>
    <xsl:value-of select="../group"/>
   </tc:keyword>
  </tc:keywords>
 </tc:entry>
</xsl:template>

<xsl:template match="titles">
 <tc:title>
  <!-- ignore multiple titles -->
  <xsl:value-of select="title[1]"/>
 </tc:title>
</xsl:template>

<xsl:template match="issue_number">
 <tc:issue>
  <xsl:value-of select="."/>
 </tc:issue>
</xsl:template>

<xsl:template match="purchase_price">
 <tc:pur_price>
  <xsl:value-of select="."/>
 </tc:pur_price>
</xsl:template>

<xsl:template match="comment">
 <tc:comments>
  <xsl:value-of select="."/>
 </tc:comments>
</xsl:template>

<xsl:template match="location">
 <tc:location>
  <xsl:value-of select="."/>
 </tc:location>
</xsl:template>

<xsl:template match="condition">
 <tc:condition i18n="yes">
  <xsl:choose>
   <xsl:when test=".='0'"><xsl:text>Mint</xsl:text></xsl:when>
   <xsl:when test=".='1'"><xsl:text>Near Mint</xsl:text></xsl:when>
   <xsl:when test=".='2'"><xsl:text>Very Fine</xsl:text></xsl:when>
   <xsl:when test=".='3'"><xsl:text>Fine</xsl:text></xsl:when>
   <xsl:when test=".='4'"><xsl:text>Very Good</xsl:text></xsl:when>
   <xsl:when test=".='5'"><xsl:text>Good</xsl:text></xsl:when>
   <xsl:when test=".='6'"><xsl:text>Fair</xsl:text></xsl:when>
   <xsl:when test=".='7'"><xsl:text>Poor</xsl:text></xsl:when>
  </xsl:choose>
 </tc:condition>
</xsl:template>

<xsl:template match="image">
 <tc:cover>
  <xsl:value-of select="."/>
 </tc:cover>
</xsl:template>

<xsl:template match="date">
 <tc:pub_year>
  <!-- default seems to be stored as seconds since epoch? -->
  <!-- accept round-off error and leap years -->
  <xsl:if test=". &lt; 2100">
   <xsl:value-of select="."/>
  </xsl:if>
  <xsl:if test="not(. &lt; 2100)">
   <xsl:value-of select="1970 + floor(. div (365*24*60*60))"/>
  </xsl:if>
 </tc:pub_year>
</xsl:template>

<xsl:template match="writers">
 <tc:writers>
  <xsl:for-each select="writer">
   <tc:writer><xsl:value-of select="."/></tc:writer>
  </xsl:for-each>
 </tc:writers>
</xsl:template>

</xsl:stylesheet>
