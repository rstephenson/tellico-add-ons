<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tc="http://periapsis.org/tellico/"
                exclude-result-prefixes="tc"
                version="1.0">

<!--
   ===================================================================
   Tellico XSLT file - template for viewing entry data

   Copyright (C) 2003-2006 Robby Stephenson - robby@periapsis.org

   The drop-shadow effect is based on the "A List Apart" method
   at http://www.alistapart.com/articles/cssdropshadows/

   This XSLT stylesheet is designed to be used with the 'Tellico'
   application, which can be found at http://www.periapsis.org/tellico/
   ===================================================================
-->

<!-- import common templates -->
<!-- location depends on being installed correctly -->
<xsl:import href="../tellico-common.xsl"/>

<xsl:output method="html" version="xhtml"/>

<xsl:param name="datadir"/> <!-- dir where Tellico data are located -->
<xsl:param name="imgdir"/> <!-- dir where field images are located -->
<xsl:param name="font"/> <!-- default KDE font family -->
<xsl:param name="fontsize"/> <!-- default KDE font size -->

<xsl:param name="collection-file"/> <!-- might have a link to parent collection -->

<xsl:key name="fieldsByName" match="tc:field" use="@name"/>
<xsl:key name="fieldsByCat" match="tc:field" use="@category"/>
<xsl:key name="imagesById" match="tc:image" use="@id"/>

<xsl:variable name="numcols" select="'2'"/>
<xsl:variable name="image-width" select="'150'"/>
<xsl:variable name="image-height" select="'200'"/>
<xsl:variable name="endl">
<xsl:text>
</xsl:text>
</xsl:variable>

<!-- all the categories -->
<xsl:variable name="categories" select="/tc:tellico/tc:collection/tc:fields/tc:field[generate-id(.)=generate-id(key('fieldsByCat',@category)[1])]/@category"/>
<!-- layout changes depending on whether there are images or not -->
<xsl:variable name="num-images" select="count(tc:tellico/tc:collection/tc:entry[1]/*[key('fieldsByName',local-name(.))/@type = 10])"/>

<xsl:template match="/">
 <xsl:apply-templates select="tc:tellico"/>
</xsl:template>

<!-- The default layout is pretty boring, but catches every field value in
     the entry. The title is in the top H1 element. -->
<xsl:template match="tc:tellico">
 <!-- This stylesheet is designed for Tellico document syntax version 9 -->
 <xsl:call-template name="syntax-version">
  <xsl:with-param name="this-version" select="'9'"/>
  <xsl:with-param name="data-version" select="@syntaxVersion"/>
 </xsl:call-template>

 <html>
  <head>
  <style type="text/css">
html,body {
    margin:0;
    padding:0;
    color:#000;
    background:#fff;
}
body {
    font-family: "<xsl:value-of select="$font"/>", Arial, Helvetica, sans-serif;
    padding-top: 10px;
    font-size: <xsl:value-of select="$fontsize"/>pt;
}
#images {
    margin-left: -8px;
    margin-right: 10px;
    float: right;
}
h3 {
    text-align: center;
    font-size: 1.1em;
}
div.category {
    background:#fff;
    padding: 0px 0px 0px 4px;
    text-align: center;
    min-height: 1em;
    overflow: hidden;
}
table {
    border-collapse: collapse;
    border-spacing: 0px;
    width: 100%;
    font-size:90%;
}
tr.table-columns {
    font-style: italic;
}
th.fieldName {
    font-weight: bolder;
    text-align: left;
    padding: 0px 4px 0px 2px;
    white-space: nowrap;
    width: 25%;
}
td.fieldValue {
    text-align: left;
    padding: 0px 10px 0px 2px;
    width: 25%;
}
td.column1 {
    font-weight: bold;
    text-align: left;
    padding: 0px 2px 0px 2px;
}
td.column2 {
    font-style: italic;
    text-align: left;
    padding: 0px 10px 0px 10px;
}
p {
    margin: 2px 10px 2px 0;
    padding: 0px;
    text-align: justify;
    font-size: 90%;
}
ul {
    text-align: left;
    margin: 0px 0px 0px 0px;
    padding-left: 20px;
}
img {
    margin: 0;
    padding:0;
    border: 0;
}
/* CSS for the box starts here
================================================*/
.cbb {
    background-image:url('Berean_Curves/top-no-trans.gif'), url('Berean_Curves/bottom-no-trans.gif'), url('Berean_Curves/borders-no-trans.gif'), url('Berean_Curves/borders-no-trans.gif');
    background-position:100% 0, 100% 100%, 0 0, 100% 0;
    background-repeat:no-repeat, no-repeat, repeat-y, repeat-y;
    padding:0px 12px;
    margin: 0px 10px;
    overflow: hidden;
}
.cbb:before {
    content:"";
    display:block;
    height:17px;
    width:18px;
    background:url('Berean_Curves/top-no-trans.gif') no-repeat 0 0;
    margin:0 0 0 -12px;
}
.cbb:after {
    content:"";
    display:block;
    height:14px;
    width:18px;
    background:url('Berean_Curves/bottom-no-trans.gif') no-repeat 0 100%;
    margin:0 0 0 -12px;
}
.cbb h1 {
    margin: 0;
    padding-top: 2px;
    padding-bottom: 0;
    background: #efece6;
    text-align: center;
    font-size: 150%;
}
.cbb h2 {
    margin: 0 -10px;
    margin-bottom: 4px;
    padding-bottom: 0;
    background: #efece6;
    font-size: 110%;
}
/* CSS for the box ends here */
  </style>
  <title>
   <xsl:value-of select="tc:collection[1]/tc:entry[1]//tc:title[1]"/>
   <xsl:text> - </xsl:text>
   <xsl:value-of select="tc:collection[1]/@title"/>
  </title>
  </head>
  <body>
   <xsl:apply-templates select="tc:collection[1]"/>
  </body>
 </html>
</xsl:template>

<xsl:template match="tc:collection">
 <xsl:apply-templates select="tc:entry[1]"/>
</xsl:template>

<xsl:template match="tc:entry">
 <div class="cbb">
  <h1>
   <xsl:value-of select=".//tc:title[1]"/>
  </h1>
 </div>
 <xsl:variable name="entry" select="."/>

 <!-- all the images are in a div, aligned to the right side and floated -->
 <!-- only want this div if there are any images in the entry -->
 <xsl:if test="$num-images &gt; 0">
  <div id="images" class="cbb">
   <!-- images are field type 10 -->
   <xsl:for-each select="../tc:fields/tc:field[@type=10]">
    <!-- find the value of the image field in the entry -->
    <xsl:variable name="image" select="$entry/*[local-name(.) = current()/@name]"/>
    <!-- check if the value is not empty -->
    <xsl:if test="$image">
     <img style="vertical-align:bottom;">
      <xsl:attribute name="src">
       <xsl:value-of select="concat($imgdir, $image)"/>
      </xsl:attribute>
      <!-- limit to maximum width of 150 and height of 200 -->
      <xsl:call-template name="image-size">
       <xsl:with-param name="limit-width" select="$image-width"/>
       <xsl:with-param name="limit-height" select="$image-height"/>
       <xsl:with-param name="image" select="key('imagesById', $image)"/>
      </xsl:call-template>
     </img>
    </xsl:if>
   </xsl:for-each>
  </div>
 </xsl:if>

  <!-- now for all the rest of the data -->
  <!-- iterate over the categories, but skip paragraphs and images -->
  <xsl:for-each select="$categories[key('fieldsByCat',.)[1]/@type != 2 and key('fieldsByCat',.)[1]/@type != 10]">
   <xsl:call-template name="output-category">
    <xsl:with-param name="entry" select="$entry"/>
    <xsl:with-param name="category" select="."/>
   </xsl:call-template>
  </xsl:for-each>

  <!-- now do paragraphs -->
  <xsl:for-each select="$categories[key('fieldsByCat',.)[1]/@type = 2]">
   <xsl:call-template name="output-category">
    <xsl:with-param name="entry" select="$entry"/>
    <xsl:with-param name="category" select="."/>
   </xsl:call-template>
  </xsl:for-each>

</xsl:template>

<xsl:template name="output-category">
 <xsl:param name="entry"/>
 <xsl:param name="category"/>

 <xsl:variable name="fields" select="key('fieldsByCat', $category)"/>
 <xsl:variable name="first-type" select="$fields[1]/@type"/>

 <xsl:variable name="n" select="count($entry//*[key('fieldsByName',local-name(.))/@category=$category])"/>
 <!-- only output if there are fields in this category, or if there are no images
      also, special case, don't output empty paragraphs -->
 <xsl:if test="($n &gt; 0 or $num-images = 0) and (not($first-type = 2) or $entry/*[local-name(.) = $fields[1]/@name])">
 <div class="cbb">
  <div class="category">

   <h2>
    <xsl:value-of select="$category"/>
   </h2>
   <!-- ok, big xsl:choose loop for field type -->
   <xsl:choose>

    <!-- paragraphs are field type 2 -->
    <xsl:when test="$first-type = 2">
     <p>
      <!-- disabling the output escaping allows html -->
      <xsl:value-of select="$entry/*[local-name(.) = $fields[1]/@name]" disable-output-escaping="yes"/>
     </p>
    </xsl:when>

    <!-- tables are field type 8 -->
    <!-- ok to put category name inside div instead of table here -->
    <xsl:when test="$first-type = 8">
     <!-- look at number of columns -->
     <xsl:choose>
      <xsl:when test="$fields[1]/tc:prop[@name = 'columns'] &gt; 1">
       <table width="100%">
        <xsl:if test="$fields[1]/tc:prop[@name = 'column1']">
         <thead>
          <tr class="table-columns">
           <th width="50%">
            <xsl:value-of select="$fields[1]/tc:prop[@name = 'column1']"/>
           </th>
           <th width="50%">
            <xsl:value-of select="$fields[1]/tc:prop[@name = 'column2']"/>
           </th>
           <xsl:call-template name="columnTitle">
            <xsl:with-param name="index" select="3"/>
            <xsl:with-param name="max" select="$fields[1]/tc:prop[@name = 'columns']"/>
            <xsl:with-param name="elem" select="'th'"/>
            <xsl:with-param name="field" select="$fields[1]"/>
           </xsl:call-template>
          </tr>
         </thead>
        </xsl:if>
        <tbody>
         <xsl:for-each select="$entry//*[local-name(.) = $fields[1]/@name]">
          <tr>
           <xsl:for-each select="tc:column">
            <xsl:choose>
             <xsl:when test="position() = 1">
              <td class="column1">
               <xsl:value-of select="."/>
               <xsl:text>&#160;</xsl:text>
              </td>
             </xsl:when>
             <xsl:otherwise>
              <td class="column2">
               <xsl:value-of select="."/>
               <xsl:text>&#160;</xsl:text>
              </td>
             </xsl:otherwise>
            </xsl:choose>
           </xsl:for-each>
          </tr>
         </xsl:for-each>
        </tbody>
       </table>
      </xsl:when>
      <xsl:otherwise>
       <ul>
        <xsl:for-each select="$entry//*[local-name(.) = $fields[1]/@name]">
         <li>
          <xsl:value-of select="."/>
         </li>
        </xsl:for-each>
       </ul>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>

    <xsl:when test="$first-type = 10"/>

    <xsl:otherwise>
     <table>
      <tbody>
       <xsl:for-each select="$fields[(position()-1) mod $numcols = 0]">
        <tr>
         <xsl:call-template name="output-field">
          <xsl:with-param name="field" select="."/>
          <xsl:with-param name="entry" select="$entry"/>
         </xsl:call-template>
         <xsl:variable name="pos" select="position()"/>
         <xsl:for-each select="$fields[position() &gt; $numcols*($pos - 1)+1 and position() &lt;= $numcols*$pos]">
          <xsl:call-template name="output-field">
           <xsl:with-param name="field" select="."/>
           <xsl:with-param name="entry" select="$entry"/>
          </xsl:call-template>
         </xsl:for-each>
        </tr>
       </xsl:for-each>
      </tbody>
     </table>

    </xsl:otherwise>
   </xsl:choose>

  </div>
</div>
 </xsl:if>
</xsl:template>

<xsl:template name="output-field">
 <xsl:param name="field"/>
 <xsl:param name="entry"/>

 <th class="fieldName" valign="top">
  <xsl:value-of select="$field/@title"/>
  <xsl:text>:</xsl:text>
 </th>
 <td class="fieldValue">
  <xsl:call-template name="simple-field-value">
   <xsl:with-param name="entry" select="$entry"/>
   <xsl:with-param name="field" select="$field/@name"/>
  </xsl:call-template>
 </td>
</xsl:template>

</xsl:stylesheet>
