<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:tc="http://periapsis.org/tellico/"
                exclude-result-prefixes="tc"
                version="1.0">

<!--
   ===================================================================
   Tellico XSLT file - reinhardt template
   Based on amaroK theme by same name

   Copyright (C) 2003-2006 Robby Stephenson - robby@periapsis.org

   The drop-shadow effect is based on the "A List Apart" method
   at http://www.alistapart.com/articles/cssdropshadows/

   This XSLT stylesheet is designed to be used with the 'Tellico'
   application, which can be found at http://www.periapsis.org/tellico/
   ===================================================================
-->

<!-- import common templates -->
<!-- location depends on being installed correctly -->
<xsl:import href="../tellico-common.xsl"/>

<xsl:output method="html"
            indent="yes"
            doctype-public="-//W3C//DTD HTML 4.01//EN"
            doctype-system="http://www.w3.org/TR/html4/strict.dtd"
            encoding="utf-8"/>

<xsl:param name="datadir"/> <!-- dir where Tellico data are located -->
<xsl:param name="imgdir"/> <!-- dir where field images are located -->
<xsl:param name="font"/> <!-- font family -->
<xsl:param name="fontsize"/> <!-- font size -->

<xsl:param name="collection-file"/> <!-- might have a link to parent collection -->

<xsl:key name="fieldsByName" match="tc:field" use="@name"/>
<xsl:key name="fieldsByCat" match="tc:field" use="@category"/>
<xsl:key name="imagesById" match="tc:image" use="@id"/>

<xsl:variable name="numcols" select="'2'"/>
<xsl:variable name="image-width" select="'150'"/>
<xsl:variable name="image-height" select="'200'"/>
<xsl:variable name="endl">
<xsl:text>
</xsl:text>
</xsl:variable>

<!-- all the categories -->
<xsl:variable name="categories" select="/tc:tellico/tc:collection/tc:fields/tc:field[generate-id(.)=generate-id(key('fieldsByCat',@category)[1])]/@category"/>
<!-- layout changes depending on whether there are images or not -->
<xsl:variable name="num-images" select="count(tc:tellico/tc:collection/tc:entry[1]/*[key('fieldsByName',local-name(.))/@type = 10])"/>

<xsl:template match="/">
 <xsl:apply-templates select="tc:tellico"/>
</xsl:template>

<!-- The default layout is pretty boring, but catches every field value in
     the entry. The title is in the top H1 element. -->
<xsl:template match="tc:tellico">
 <!-- This stylesheet is designed for Tellico document syntax version 9 -->
 <xsl:call-template name="syntax-version">
  <xsl:with-param name="this-version" select="'9'"/>
  <xsl:with-param name="data-version" select="@syntaxVersion"/>
 </xsl:call-template>

 <html>
  <head>
  <style type="text/css">
  body {
    margin: 0px;
    padding: 0px;
    font-family: "<xsl:value-of select="$font"/>";
    font-size: <xsl:value-of select="$fontsize"/>pt;
    color: black;
    background-color: white;
    background-image: url(utvols/background.png);
    background-repeat: no-repeat;
    background-position: top center;
  }
  h1 {
    margin: 0px;
    padding: 0px;
    font-size: 2em;
    background-color: white;
    color: #e87b07;
    text-align: center;
  }
  <xsl:if test="$num-images &gt; 0">
  div#images {
    margin: 10px 5px 0px 5px;
    float: right;
    min-width: <xsl:value-of select="$image-width"/>px; /* min-width instead of width, stylesheet actually scales image */
  }
  </xsl:if>
  div.img-shadow {
    float: left;
    background: url(<xsl:value-of select="concat($datadir,'shadowAlpha.png')"/>) no-repeat bottom right;
    margin: 6px 0 10px 10px;
    clear: left;
  }
  div.img-shadow img {
    display: block;
    position: relative;
    border: 1px solid #a9a9a9;
    margin: -6px 6px 6px -6px;
  }
  h3 {
    text-align: center;
    font-size: 1.1em;
  }
  div#content {
    padding-left: 1%;
    padding-right: 1%;
  }
  div.category {
    padding: 0px 0px 0px 4px;
    margin: 8px;
    text-align: center;
    min-height: 1em;
    overflow: hidden;
  }
  h2 {
    background-color: transparent;
    color: #e87b07;
    border-bottom: 1px outset;
    padding: 0px 8px 0px 0px;
    margin: 0px 0px 2px -4px; /* -4px to match .category padding */
    font-size: 1.0em;
    top: -1px;
    font-style: normal;
    text-align: left;
  }
  table {
    border-collapse: collapse;
    border-spacing: 0px;
    width: 100%;
  }
  tr.table-columns {
    font-style: italic;
  }
  th.fieldName {
    font-weight: bolder;
    text-align: left;
    padding: 0px 4px 0px 2px;
    white-space: nowrap;
    width: 25%;
  }
  td.fieldValue {
    text-align: left;
    padding: 0px 10px 0px 2px;
    width: 25%;
    vertical-align: top;
  }
  td.column1 {
    font-weight: bold;
    text-align: left;
    padding: 0px 2px 0px 2px;
  }
  td.column2 {
    font-style: italic;
    text-align: left;
    padding: 0px 10px 0px 10px;
  }
  p {
    margin: 2px 10px 2px 0;
    padding: 0px;
    text-align: justify;
    font-size: 90%;
  }
  ul {
    text-align: left;
    margin: 0px 0px 0px 0px;
    padding-left: 20px;
  }
  img {
    border: 0px;
  }
  p.navigation {
    font-weight: bold;
    text-align: center;
    clear: both;
  }
  </style>
  <title>
   <xsl:value-of select="tc:collection[1]/tc:entry[1]//tc:title[1]"/>
   <xsl:text>&#xa0;&#8211; </xsl:text>
   <xsl:value-of select="tc:collection[1]/@title"/>
  </title>
  </head>
  <body>
   <xsl:apply-templates select="tc:collection[1]"/>
   <xsl:if test="$collection-file">
    <p class="navigation">
     <a href="{$collection-file}">&lt;&lt; <xsl:value-of select="tc:collection/@title"/></a>
    </p>
   </xsl:if>
  </body>
 </html>
</xsl:template>

<xsl:template match="tc:collection">
 <xsl:apply-templates select="tc:entry[1]"/>
</xsl:template>

<xsl:template match="tc:entry">
 <xsl:variable name="entry" select="."/>

 <!-- first, show the title -->
 <xsl:if test=".//tc:title">
  <h1>
   <img src="utvols/helmet-right.png" width="73" height="48" style="vertical-align: bottom"/>
   <xsl:value-of select=".//tc:title[1]"/>
   <img src="utvols/helmet-left.png" width="73" height="48" style="vertical-align: bottom"/>
  </h1>
 </xsl:if>

 <!-- all the images are in a div, aligned to the right side and floated -->
 <!-- only want this div if there are any images in the entry -->
 <xsl:if test="$num-images &gt; 0">
  <div id="images">
   <!-- images are field type 10 -->
   <xsl:for-each select="../tc:fields/tc:field[@type=10]">

    <!-- find the value of the image field in the entry -->
    <xsl:variable name="image" select="$entry/*[local-name(.) = current()/@name]"/>
    <!-- check if the value is not empty -->
    <xsl:if test="$image">
     <div class="img-shadow">
      <a>
       <xsl:attribute name="href">
        <xsl:choose>
         <!-- Amazon license requires the image to be linked to the amazon website -->
         <xsl:when test="$entry/tc:amazon">
          <xsl:value-of select="$entry/tc:amazon"/>
         </xsl:when>
         <xsl:otherwise>
          <xsl:value-of select="concat($imgdir, $image)"/>
         </xsl:otherwise>
        </xsl:choose>
       </xsl:attribute>
       <img alt="" style="vertical-align:bottom;">
        <xsl:attribute name="src">
         <xsl:value-of select="concat($imgdir, $image)"/>
        </xsl:attribute>
        <!-- limit to maximum width of 150 and height of 200 -->
        <xsl:call-template name="image-size">
         <xsl:with-param name="limit-width" select="$image-width"/>
         <xsl:with-param name="limit-height" select="$image-height"/>
         <xsl:with-param name="image" select="key('imagesById', $image)"/>
        </xsl:call-template>
       </img>
      </a>
     </div>
     <br/> <!-- needed since the img-shadow block floats -->
    </xsl:if>
   </xsl:for-each>
  </div>
 </xsl:if>

 <!-- all the data is in the content block -->
 <div id="content">
  <!-- now for all the rest of the data -->
  <!-- iterate over the categories, but skip paragraphs and images -->
  <xsl:for-each select="$categories[key('fieldsByCat',.)[1]/@type != 2 and key('fieldsByCat',.)[1]/@type != 10]">
   <xsl:call-template name="output-category">
    <xsl:with-param name="entry" select="$entry"/>
    <xsl:with-param name="category" select="."/>
   </xsl:call-template>
  </xsl:for-each>

  <!-- now do paragraphs -->
  <xsl:for-each select="$categories[key('fieldsByCat',.)[1]/@type = 2]">
   <xsl:call-template name="output-category">
    <xsl:with-param name="entry" select="$entry"/>
    <xsl:with-param name="category" select="."/>
   </xsl:call-template>
  </xsl:for-each>

 </div>

</xsl:template>

<xsl:template name="output-category">
 <xsl:param name="entry"/>
 <xsl:param name="category"/>

 <xsl:variable name="fields" select="key('fieldsByCat', $category)"/>
 <xsl:variable name="first-type" select="$fields[1]/@type"/>

 <xsl:variable name="n" select="count($entry//*[key('fieldsByName',local-name(.))/@category=$category])"/>
 <!-- only output if there are fields in this category
      also, special case, don't output empty paragraphs -->
 <xsl:if test="$n &gt; 0 and (not($first-type = 2) or $entry/*[local-name(.) = $fields[1]/@name])">
 <div class="container">
  <xsl:if test="$num-images = 0">
   <xsl:attribute name="style">
    <xsl:text>width: 100%;</xsl:text>
   </xsl:attribute>
  </xsl:if>
  <div class="category">

   <h2>
    <xsl:value-of select="$category"/>
   </h2>
   <!-- ok, big xsl:choose loop for field type -->
   <xsl:choose>

    <!-- paragraphs are field type 2 -->
    <xsl:when test="$first-type = 2">
     <p>
      <!-- disabling the output escaping allows html -->
      <xsl:value-of select="$entry/*[local-name(.) = $fields[1]/@name]" disable-output-escaping="yes"/>
     </p>
    </xsl:when>

    <!-- tables are field type 8 -->
    <!-- ok to put category name inside div instead of table here -->
    <xsl:when test="$first-type = 8">
     <!-- look at number of columns -->
     <xsl:choose>
      <xsl:when test="$fields[1]/tc:prop[@name = 'columns'] &gt; 1">
       <table width="100%">
        <xsl:if test="$fields[1]/tc:prop[@name = 'column1']">
         <thead>
          <tr class="table-columns">
           <th width="50%">
            <xsl:value-of select="$fields[1]/tc:prop[@name = 'column1']"/>
           </th>
           <th width="50%">
            <xsl:value-of select="$fields[1]/tc:prop[@name = 'column2']"/>
           </th>
           <xsl:call-template name="columnTitle">
            <xsl:with-param name="index" select="3"/>
            <xsl:with-param name="max" select="$fields[1]/tc:prop[@name = 'columns']"/>
            <xsl:with-param name="elem" select="'th'"/>
            <xsl:with-param name="field" select="$fields[1]"/>
           </xsl:call-template>
          </tr>
         </thead>
        </xsl:if>
        <tbody>
         <xsl:for-each select="$entry//*[local-name(.) = $fields[1]/@name]">
          <tr>
           <xsl:for-each select="tc:column">
            <xsl:choose>
             <xsl:when test="position() = 1">
              <td class="column1">
               <xsl:value-of select="."/>
               <xsl:text>&#160;</xsl:text>
              </td>
             </xsl:when>
             <xsl:otherwise>
              <td class="column2">
               <xsl:value-of select="."/>
               <xsl:text>&#160;</xsl:text>
              </td>
             </xsl:otherwise>
            </xsl:choose>
           </xsl:for-each>
          </tr>
         </xsl:for-each>
        </tbody>
       </table>
      </xsl:when>
      <xsl:otherwise>
       <ul>
        <xsl:for-each select="$entry//*[local-name(.) = $fields[1]/@name]">
         <li>
          <xsl:value-of select="."/>
         </li>
        </xsl:for-each>
       </ul>
      </xsl:otherwise>
     </xsl:choose>
    </xsl:when>

    <xsl:otherwise>
     <table>
      <tbody>
       <xsl:for-each select="$fields[(position()-1) mod $numcols = 0]">
        <tr>
         <xsl:call-template name="output-field">
          <xsl:with-param name="field" select="."/>
          <xsl:with-param name="entry" select="$entry"/>
         </xsl:call-template>
         <xsl:variable name="pos" select="position()"/>
         <xsl:for-each select="$fields[position() &gt; $numcols*($pos - 1)+1 and position() &lt;= $numcols*$pos]">
          <xsl:call-template name="output-field">
           <xsl:with-param name="field" select="."/>
           <xsl:with-param name="entry" select="$entry"/>
          </xsl:call-template>
         </xsl:for-each>
        </tr>
       </xsl:for-each>
      </tbody>
     </table>

    </xsl:otherwise>
   </xsl:choose>

  </div>
  </div>
 </xsl:if>
</xsl:template>

<xsl:template name="output-field">
 <xsl:param name="field"/>
 <xsl:param name="entry"/>

 <th class="fieldName" valign="top">
  <xsl:value-of select="$field/@title"/>
  <xsl:text>:</xsl:text>
 </th>
 <td class="fieldValue">
  <xsl:call-template name="simple-field-value">
   <xsl:with-param name="entry" select="$entry"/>
   <xsl:with-param name="field" select="$field/@name"/>
  </xsl:call-template>
 </td>
</xsl:template>

</xsl:stylesheet>
