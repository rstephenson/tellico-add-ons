Name=Open ILS Supercat Search
Type=data-source
ArgumentKeys=1,2,3,5
Arguments=-t %1,-a %1,-i %1,-k %1
CollectionType=2
FormatType=6
UpdateArgs=-t %{title} -a %{artist} -i %{isbn}
