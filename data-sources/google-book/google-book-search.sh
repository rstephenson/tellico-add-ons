#!/bin/sh

dir=`dirname $0`
query="$1"
url="http://books.google.com/books/feeds/volumes?q='${query}'&start-index=1&max-results=25"

#echo $url;

wget "$url" --quiet --output-document=- | xsltproc ${dir}/gb2tellico.xsl -
